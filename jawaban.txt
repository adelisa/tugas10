1. Buat Database:
    create database myshop;

2. Buat Table dlm Database:
    create table users (
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

    create table categories (
    -> id int auto_increment primary key,
    -> name varchar(255)
    -> );

    create table items (
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key (category_id) references categories (id)
    -> );

3. Memasukkan Data pd Table:
    insert into users value
    -> ('1','John Doe','john@doe.com','doejohn'),
    -> ('2','John Doe','john@doe.com','johndoe')
    -> ;

    insert into categories values
    -> ('1','Gadget'),
    -> ('2','Cloth'),
    -> ('3','Men'),
    -> ('4','Women'),
    -> ('5','Branded')
    -> ;

    insert into items values
    -> ('1','Sumsang b50','Hape keren dari merek sumsang','1999000','100','1'),
    -> ('2','Uniklooh','Baju keren dan unik dari brand ternama','200000','100','2'),
    -> ('3','IMHO Watch','Jam tangan anak yang jujur banget','1100000','100','5')
    -> ;

4. Mengambil Data dari Database:
    a. Mengambil data users
        select id, name, email from users;

    b. Mengambil data items
       *. select *from items where price > 1000000;
       *. select *from items where name like '%uniklo%';

    c. Menampilkan Data Items join dg kategori
        SELECT items.name, items.description, categories.name AS category_name
    -> FROM items
    -> JOIN categories ON items.category_id = categories.id;

5. Mengubah Data dari Database:
    update items set price='2500000' where id='1';
